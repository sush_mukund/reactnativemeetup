import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';
import {Actions} from 'react-native-router-flux';

export default class HomePage extends Component {

  constructor() {
    super();
  }

  onPress() {
    console.log("onPress");
    Actions.todo({data:"data"});
  }

  render() {
    return (
      <View style={styles.container}>
        <Button
          title="Let's get started!" onPress={this.onPress.bind(this)} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
      row: {
        flexDirection:'row',
        justifyContent:'center',
        padding:10,
        backgroundColor: '#f4f4f4',
        marginBottom:3
    },
    rowText: {
        flex:1
    }
});
