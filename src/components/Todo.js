import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button, ListView
} from 'react-native';

export default class Todo extends Component {

  constructor() {
    super();
    this.state = {
      listDate: [{
        name: "Sush"
      },
      {
        name: "Sowmya"
      }]
    }
  }

  componentWillMount() {

     const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            userDataSource: ds,
        };
    
  }

  componentDidMount(){
        this.fetchUsers();
    }

    fetchUsers(){
        fetch('https://jsonplaceholder.typicode.com/users')
            .then((response) => response.json())
            .then((response) => {
                this.setState({
                    userDataSource: this.state.userDataSource.cloneWithRows(response)
                });
            });
    }

  renderRow(user, sectionId, rowId, highlightRow){
        return(
            // <TouchableHighlight onPress={() => {this.onPress(user)}}>
            <View style={styles.row}>
                <Text style={styles.rowText}>{user.name}: {user.email}</Text>
            </View>
            // </TouchableHighlight>
        )
    }

    render(){
        return(
        <ListView 
            dataSource={this.state.userDataSource}
            renderRow={this.renderRow.bind(this)}
        />
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  }
});
