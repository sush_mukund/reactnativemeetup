import React, { Component } from 'react';
import { Scene, Router } from 'react-native-router-flux';
import HomePage from './src/components/HomePage'
import Todo from './src/components/Todo'

export default class App extends Component {

  render() {
    return (
      <Router>
        <Scene key="root">
            <Scene key="homPage" component={HomePage} title="HomePage" />
            <Scene key="todo" component={Todo} title="Todo" />
        </Scene>
      </Router>
    );
  }
}

